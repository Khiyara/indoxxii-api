from django.urls import path, include
from django.conf.urls import url
from rest_framework import routers, serializers, viewsets
from .views import ListTheatreView, ListTicketView, ListCustomerView, ListMovieView, ListSeatsView, ListAuditView, ListStaffView, CustomerViewSet, TicketViewSet, TheatreViewSet, MovieViewSet, SeatsViewSet, AuditViewSet, StaffViewSet


urlpatterns = [
    path('theatre', ListTheatreView.as_view(), name="theatre-all"),
    path('ticket', ListTicketView.as_view(), name="theatre-all"),
    path('customer', ListCustomerView.as_view(), name="theatre-all"),
    path('movie', ListMovieView.as_view(), name="theatre-all"),
    path('seats', ListSeatsView.as_view(), name="theatre-all"),
    path('audit', ListAuditView.as_view(), name="theatre-all"),
    path('staff', ListStaffView.as_view(), name="theatre-all")]


router = routers.DefaultRouter()
router.register(r'customer', CustomerViewSet)
router.register(r'ticket', TicketViewSet)
router.register(r'theatre', TheatreViewSet)
router.register(r'movie', MovieViewSet)
router.register(r'seats', SeatsViewSet)
router.register(r'audit', AuditViewSet)
router.register(r'staff', StaffViewSet)
api_urlpatterns = ([
    url('', include(router.urls)),
], 'api')
urlpatterns += [
    url(r'^data/', include(api_urlpatterns)),
]