from django.contrib import admin
from .models import Theatre,Customer, Movie, Seats, Audit, Staff, Ticket

admin.site.register(Theatre)
admin.site.register(Customer)
admin.site.register(Movie)
admin.site.register(Seats)
admin.site.register(Audit)
admin.site.register(Staff)
admin.site.register(Ticket)
