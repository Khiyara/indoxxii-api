from rest_framework import routers, serializers, viewsets
from .models import Theatre, Customer, Movie, Seats, Audit, Staff, Ticket
from rest_framework.response import Response

        
class TicketSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        print(validated_data)
        return Ticket.objects.create(**validated_data)
        
    class Meta:
        model = Ticket
        fields = "__all__"
        depth = 1
        
class CustomerSerializer(serializers.ModelSerializer):
    ticket_set = TicketSerializer(many=True,source = "ticket_bought", read_only=True)
    
    def create(self, validated_data):
        print(validated_data)
        customer = Customer.objects.create(**validated_data)
        return customer
        
    class Meta:
        model = Customer
        fields = ("customer_id", "customer_email", "customer_name", "customer_gender", "custoemr_age", "customer_phone_number", "ticket_set")
        depth = 1  
        
class SeatsSerializer(serializers.ModelSerializer):
    ticket_seats = TicketSerializer(many=True, read_only=True)
    
    def create(self, validated_data):
        print(validated_data)
        customer = Seats.objects.create(**validated_data)
        return customer
    class Meta:
        model = Seats
        fields = "__all__"   
        
class AuditSerializer(serializers.ModelSerializer):
    ticket_audit = TicketSerializer(many=True, read_only=True)
    seats = SeatsSerializer(many=True,source = "seats_audit", read_only=True)
    def create(self, validated_data):
        print(validated_data)
        customer = Audit.objects.create(**validated_data)
        return customer
    class Meta:
        model = Audit
        fields = ("audit_theatre", "audit_number", "seats", "ticket_audit")
        
class TheatreSerializer(serializers.ModelSerializer):
    audit_set = AuditSerializer(many=True,source = "audit_theatre", read_only=True)
    
    def create(self, validated_data):
        print(validated_data)
        customer = Theatre.objects.create(**validated_data)
        return customer
        
    class Meta:
        model = Theatre
        description = "List of theatre_name"
        fields = ("theatre_name", "theatre_place", "audit_set")
        
class MovieSerializer(serializers.ModelSerializer):
    theatre_set = TheatreSerializer(many=True, source = "theatre_movie", read_only=True)
    def create(self, validated_data):
        print(validated_data)
        customer = Movie.objects.create(**validated_data)
        return customer
    class Meta:
        model = Movie
        fields = "__all__"   
        
class StaffSerializer(serializers.ModelSerializer):
    ticket_set = TicketSerializer(many=True,source = "ticket_handled_by")
    theatre_set = TheatreSerializer(many=True, source = "worked_staff")
    
    def create(self, validated_data):
        print(validated_data)
        customer = Staff.objects.create(**validated_data)
        return customer
    class Meta:
        model = Staff
        fields = "__all__"
        
"""    
class CustomerSerializer(serializers.Serializer):
    customer_email = serializers.EmailField()
    customer_name = serializers.CharField(max_length=30)

    def create(self, validated_data):
        return Customer.objects.create(**validated_data)
    
    def delete(self, validated_data):
        customer = Customer.objects.filter(**validated_data)
        customer.delete()
    """
    
"""{
    "customer_email": "pepegagiga@gmail.com",
    "customer_name": "pepegagiga",
    "customer_gender": "F",
    "custoemr_age": 10,
    "customer_phone_number": "999999",
    "ticket_set": []
}"""