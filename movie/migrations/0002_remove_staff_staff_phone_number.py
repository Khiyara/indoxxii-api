# Generated by Django 2.1.1 on 2019-05-01 14:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('movie', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='staff',
            name='staff_phone_number',
        ),
    ]
