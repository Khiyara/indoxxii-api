# Generated by Django 2.1.1 on 2019-05-01 14:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movie', '0002_remove_staff_staff_phone_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='staff',
            name='staff_phone_number',
            field=models.CharField(max_length=12, null=True, unique=True),
        ),
    ]
