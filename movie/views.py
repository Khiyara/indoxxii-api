from rest_framework import generics, viewsets, status
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Theatre, Customer, Movie, Seats, Audit, Staff, Ticket
from .serializers import TheatreSerializer, CustomerSerializer, SeatsSerializer, AuditSerializer, TicketSerializer, StaffSerializer, MovieSerializer


class ListTheatreView(generics.ListAPIView):
    
    
    
    queryset = Theatre.objects.all()
    serializer_class = TheatreSerializer

class ListCustomerView(generics.ListAPIView):
    
    
    
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer

class ListSeatsView(generics.ListAPIView):
    
    
    
    queryset = Seats.objects.all()
    serializer_class = SeatsSerializer

class ListAuditView(generics.ListAPIView):
    
    
    
    queryset = Audit.objects.all()
    serializer_class = AuditSerializer

class ListTicketView(generics.ListAPIView):
    
    
    
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer

class ListStaffView(generics.ListAPIView):
    
    
    
    queryset = Staff.objects.all()
    serializer_class = StaffSerializer
class ListMovieView(generics.ListAPIView):
    
    
    
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    
class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer

class TheatreViewSet(viewsets.ModelViewSet):
    queryset = Theatre.objects.all()
    serializer_class = TheatreSerializer

class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer

class SeatsViewSet(viewsets.ModelViewSet):
    queryset = Seats.objects.all()
    serializer_class = SeatsSerializer

class AuditViewSet(viewsets.ModelViewSet):
    queryset = Audit.objects.all()
    serializer_class = AuditSerializer

class TicketViewSet(viewsets.ModelViewSet):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer

class StaffViewSet(viewsets.ModelViewSet):
    queryset = Staff.objects.all()
    serializer_class = StaffSerializer
class MovieViewSet(viewsets.ModelViewSet):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer        
        
        
        
        
        