from django.db import models
from django import forms

class Customer(models.Model):
    customer_id = models.AutoField(primary_key=True)
    customer_email = models.EmailField(unique=True)
    customer_name = models.CharField(max_length=30)
    customer_gender = models.CharField(max_length=1)
    custoemr_age = models.IntegerField()
    customer_phone_number = models.CharField(max_length=12)
        
class Seats(models.Model):
    seats_id = models.AutoField(primary_key=True)
    seats_row = models.IntegerField()
    seats_letter = models.CharField(max_length=1)
    seats_audit = models.ForeignKey('Audit', on_delete=models.CASCADE, null=True, related_name="seats_audit")

class Audit(models.Model):
    audit_id = models.AutoField(primary_key=True)
    audit_number = models.IntegerField()
    audit_theatre = models.ForeignKey('Theatre', on_delete=models.CASCADE, null=True, related_name="audit_theatre")
    
class Ticket(models.Model):
    ticket_id = models.AutoField(primary_key=True)
    ticket_price = models.IntegerField()
    ticket_audit = models.ForeignKey(Audit, on_delete=models.CASCADE, related_name="ticket_audit", null=True)
    movie_time = models.DateField(null=True)
    ticket_buyer = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True, related_name="ticket_bought")
    ticket_seats = models.ForeignKey(Seats, on_delete=models.CASCADE, related_name="ticket_seats", null=True)
    ticket_movie = models.ForeignKey('Movie', on_delete=models.CASCADE, related_name="ticket_movie", null=True)
    ticket_handled_by = models.ForeignKey('Staff', on_delete=models.CASCADE, null=True, related_name="ticket_handled_by")

class Movie(models.Model):
    movie_id = models.AutoField(primary_key=True)
    movie_name = models.CharField(max_length=30)
    movie_genre = models.CharField(max_length=200)
    movie_length = models.IntegerField()
    movie_desc = models.CharField(max_length=999, null=True)
    movie_data = models.DateField()
    movie_now_playing = models.BooleanField(default=False)
    movie_screening = models.DateField()
    movie_image = models.CharField(max_length=999, null=True)

class Staff(models.Model):
    staff_id = models.AutoField(primary_key=True)
    staff_name = models.CharField(max_length=30)
    staff_gender = models.CharField(max_length=1)
    staff_age = models.IntegerField()
    staff_phone_number = models.CharField(max_length=12, unique=True, null=True)

class Theatre(models.Model):
    theatre_id = models.AutoField(primary_key=True)
    theatre_name = models.CharField(max_length=30)
    theatre_place = models.CharField(max_length=30)
    theatre_movie = models.ManyToManyField(Movie, null=True, related_name="theatre_movie")
    staff_work = models.ForeignKey(Staff, on_delete=models.CASCADE, null=True, related_name="worked_staff")

